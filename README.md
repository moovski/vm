# Moovski Virtual Machine

Moovski Virtual Machine repository.

## Requirements
* Please follow Moovski Dev Guide

## Getting Started

* Clone this repository.
* Run `vagrant up --provision` from the project root.

## Other commands to run
* Run `vagrant ssh` - login to vm (CTRL + D to quit)
* Run `vagrant halt` - stops your vm
* Run `vagrant destroy all` - removes whole vm
