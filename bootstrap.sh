#!/usr/bin/env bash

echo "Test 1"
ls -la /usr/local
echo "Test 2"

echo ">>> Updating Ubuntu packages"
sudo apt-get -y update
sudo apt-get -y install build-essential
sudo apt-get -y install libkrb5-dev
sudo apt-get -y install git curl vim


echo ">>> Installing Node"
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install 6.2.2
nvm alias default 6.2.2
nvm use 6.2.2
npm i -g npm


echo ">>> Installing MongoDB"
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.2 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get -y update
sudo apt-get -y install -y mongodb-org=3.2.3 mongodb-org-server=3.2.3 mongodb-org-shell=3.2.3 mongodb-org-mongos=3.2.3 mongodb-org-tools=3.2.3


echo ">>> Setup MongoDB"
cd /vagrant
mongo -u '' < createDatabase.js


echo ">>> Installing global node packages"
ls -la /usr/local
sudo chown -R $USER /usr/local
ls -la /usr/local
npm install -g --unsafe-perm install strongloop
slc
npm i -g --no-optional jshint live-server typings mocha webpack nsp aws-cli


# echo "Cloning API"
# cd /moovski
# git clone git@bitbucket.org:moovski/api.git api
# cd api
# git checkout dev
# node setup.js
# npm install
